from datetime import datetime
from colorama import Fore
R = Fore.RED
G = Fore.GREEN
C = Fore.CYAN
RT = Fore.RESET
Y = Fore.YELLOW
B = Fore.BLUE
W = Fore.WHITE


def log_time(func):
    def wrapped_func(*args, **kwargs):
        start = datetime.now()
        func(*args, **kwargs)
        out = datetime.now()
        final = str(out - start)[:7]
        print(f"{G}TimeOut: {Y}{final}{RT}", )
    return wrapped_func
