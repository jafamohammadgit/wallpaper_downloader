from starter import Starter
import argparse
from config import ALL_CATEGORIES


class Main:

    def __init__(self, categories, path, show_categories):
        self.categories = categories
        self.path = path
        self.show_categories = show_categories

    def run(self):
        if self.show_categories:
            for i in ALL_CATEGORIES:
                print(i)
        else:
            n = input("Are you sure for start? ")
            if n.lower() == 'y':
                main_start = Starter(category=self.categories, path=self.path)
                main_start.start()
            else:
                exit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Just Simple wallpaper downloader\
        from https://wallpapers.com with get category"
    )

    parser.add_argument(
        '-c', '--category',
        action='append',
        help='category wallpaper',
        type=str,
        default=ALL_CATEGORIES
    )
    parser.add_argument(
        '-p', '--path',
        type=str,
        help='Directory path to save images',
        default='./images'
    )
    parser.add_argument(
        '-s', '--show',
        help='show all categories',
        action='store_true',
        default=False
    )
    args = parser.parse_args()
    # print(args.show)
    main = Main(
        categories=args.category,
        path=args.path,
        show_categories=args.show
    )
    main.run()
