import requests
from bs4 import BeautifulSoup
from abc import ABC, abstractmethod
import wget
import os
from time import sleep


class BaseCrawler:

    @staticmethod
    def request(url):
        try:
            return requests.get(url).text
        except:
            print("ERROR: ", url)
            return None


class LinkCrawler(BaseCrawler):
    """
    Get a Url and return all links in url
    """

    def __init__(self, url, attrs=None, start_link=None):
        self.url = url
        self.attrs = attrs
        self.start_link = start_link
        self.final_link = []

    def crawler(self, html_doc):
        soup = BeautifulSoup(html_doc, 'html.parser')
        if self.attrs is not None:
            content = soup.find_all('a', attrs=self.attrs)
        else:
            content = soup.find_all("a")
        for url in content:
            if self.start_link is not None:
                self.final_link.append(self.start_link+url.get("href"))
            else:
                self.final_link.append(url.get("href"))

    def start(self):
        html_doc = self.request(self.url)
        if html_doc is not None:
            self.crawler(html_doc)
        return self.final_link


class SrcCrawler(BaseCrawler):

    def __init__(self, url, attrs=None, start_link=None, src_type='src'):
        self.url = url
        self.attrs = attrs
        self.start_link = start_link
        self.src_type = src_type

    def crawler(self, html_doc):
        soup = BeautifulSoup(html_doc, 'html.parser')
        if self.attrs is not None:
            content = soup.find_all('img', attrs=self.attrs)
        else:
            content = soup.find_all('img')
        for l in content:
            if self.start_link is not None:
                return self.start_link+l.get(self.src_type)
            else:
                return l.get(self.src_type)

    def start(self):
        html_doc = self.request(self.url)
        if html_doc is not None:
            src = str(self.crawler(html_doc))
            return src
    


