from colorama import Fore
from crawler import LinkCrawler, SrcCrawler
# from downloader import Downloader
import wget
G = Fore.GREEN
C = Fore.CYAN
RT = Fore.RESET
Y = Fore.YELLOW
W = Fore.WHITE
R = Fore.RED
B = Fore.BLUE


class Starter:

    def __init__(self, category, path):
        self.category = category
        self.path = path

    def get_categories_page_one(self, url):
        # Get all url from all categories in page one wallpapers.com
        print(f"{C}GET URL FROM PAGE: 1{RT}")
        for category in self.category:
            crawl = LinkCrawler(url=url.format(category),
                                attrs={'class': 'caption'}
                                )
            page_one = crawl.start()
        print(f"{G}Finish Page1\nNumber of category: {Y}{len(page_one)}{RT}\n")
        return page_one

    @staticmethod
    def get_categories_page_two(category_urls):
        # Get all urls from all categories in page two from wallpapers.com
        print(f'{C}GET URL FROM PAGE: 2{RT}')
        final_url = []
        for url in category_urls:
            crawl = LinkCrawler(url=url)
            page_two = crawl.start()
            for i in page_two:
                if i.endswith('.html'):
                    final_url.append(i)
        print(
            f"{G}Finish Page2\nNumber of all urls {Y}{len(final_url)}{RT}")
        return final_url

    def get_src(self, image_url):
        # get src from the urls
        print(
            f"{C}GET SRC from URLs and Downlaoding\n{RT}\n"
        )
        counter = 1
        for i in image_url:
            src_crawler = SrcCrawler(
                url=i, attrs={'class': 'post-image'},
                start_link='https://wallpapers.com',
                src_type='data-src'
            )
            src = src_crawler.start()
            self.downloader(src)
            print(f"{G}Start downloading image number {C}{counter} {G}From \
                  {C}{len(image_url)}{RT}"
                  )
            counter += 1

    def downloader(self, src):
        try:
            wget.download(url=src, out=self.path, bar=None)
            print(f"{B}Downloaded {G}✔{RT}\n")
        except:
            print(f"{R}ERROR Download!")

    def start(self):
        main_url = 'https://wallpapers.com/{}'
        page_one_urls = self.get_categories_page_one(main_url)
        page_two_urls = self.get_categories_page_two(page_one_urls)
        src = self.get_src(page_two_urls)  # get src and download
